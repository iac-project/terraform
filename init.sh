#!/usr/bin/env bash
set -e
PROJECT=12496
read -rp "Enter Gitlab username: " TF_USERNAME
read -rsp "Enter Gitlab personal token (not NTNU password): " TF_PASSWORD

TF_ADDRESS="https://${TF_USERNAME}:${TF_PASSWORD}@gitlab.stud.idi.ntnu.no/api/v4/projects/${PROJECT}/terraform/state/test_state"
terraform init \
  -backend-config=address="${TF_ADDRESS}" \
  -backend-config=lock_address="${TF_ADDRESS}"/lock \
  -backend-config=unlock_address="${TF_ADDRESS}"/lock \
  -backend-config=username="${TF_USERNAME}" \
  -backend-config=password="${TF_PASSWORD}" \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
