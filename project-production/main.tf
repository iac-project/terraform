terraform {
  required_providers {
    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }
  }
}

locals {
  cluster_name = "project-production"
}

resource "tls_private_key" "key" {
  algorithm = "RSA"
}

resource "local_file" "private_key" {
  filename = "/tmp/private_key_${local.cluster_name}"
  content  = tls_private_key.key.private_key_pem
}


module "k0s_cluster" {
  source               = "../modules/k0s-cluster"
  cluster_name         = local.cluster_name
  rsa_private_key_path = local_file.private_key.filename
  public_key_openssh   = tls_private_key.key.public_key_openssh
}

module "flux" {
  source = "../modules/flux"

  cluster_name    = local.cluster_name
  gitlab_token    = var.gitlab_token
  gitlab_username = var.gitlab_username
  kube_config     = module.k0s_cluster.kubeconfig
}