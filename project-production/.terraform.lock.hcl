# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fluxcd/flux" {
  version     = "0.6.1"
  constraints = ">= 0.0.13"
  hashes = [
    "h1:/DHKcAgp89Eqxm4Hh8DpeJwNuzwIwFWwt7uUI2ORnNI=",
    "zh:015b8e70d11e7d6b707436fbacf41028d251cabaeced05128c6e506932f9dc24",
    "zh:0222e0774de186b5c0eab1749743c92cc272dff701a8685263df3418621dec89",
    "zh:0baedbc3e7875d8a11edc5da50ddad33c35e11387e1ed19f2bf85ec38145b1d7",
    "zh:1e006bd8404d28f35970fe43774af5f824aa32589d5481a4ac4b0b4144a5222c",
    "zh:2cf555d7aad003e5d682dde8af79b367e8f6b019f8866b111a3b13bdf0f88888",
    "zh:3cab05f37902d2dc8902143f586490e8d985e7f72fc37b92ce31fd2b975c7a4d",
    "zh:40f00d64b9466df7498e6c960e127b61c8506c8dbe79e2c606a25daa5e1a57dc",
    "zh:64892b76457d17f9c3d20e0ecac45770e9814be68d292fec51fd840c6c07524c",
    "zh:7f63bf072394989cc60a90c118033682dfc2adc408244f9eb48e581744f4037f",
    "zh:96ef45354e79d5351cd18f7ac0c780ff70b5ac995a8925c82a2e45630bc7ceb3",
    "zh:daf0b70daf10dd1807aa5d16b57cb6d26d22e306918fa91e7f8bed4eed56e255",
    "zh:ee53a34473e8d169d749f1f56c22c6f97d0d2c87c4c63f42b9b08d62e2515989",
    "zh:f47d1928d273d4a92eab9c761a14a12c828feb3a2385b935c13df0f14ba9b7cd",
  ]
}

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.13.0"
  constraints = ">= 1.7.0"
  hashes = [
    "h1:rwYniZ+JW5F8U5MSRWGeXLPifGyH7mw8GNcXQyQgT9s=",
    "zh:088c99d7e079ba2be3abe1d5c5b2070eff85256178467783af125d11026f08b6",
    "zh:0d3fa3bfb4768dd39e2f3af4d85e69fdb8f6abcbe92fece37fc78a97dedd7dc1",
    "zh:227d9fb591a0cdcd482410b88c6d91f17922a85fb9caef9b73c2883f6964b483",
    "zh:607bff8e6e03ae2b4d523c21377fa655d370cc8310812310ae61b409e7c271d5",
    "zh:621d46414e23d5a7cfb1ba25275f1cac1fba78be5c1512f0a0614752425411cc",
    "zh:76aace9adb7dc9c10abcc52b31947821335b60b7b335b485bd05f20a91debd63",
    "zh:a9ff1f7c676d89cacd64605ad899749dd718f65cb879fabba8e15fcfd0a07629",
    "zh:b122fa06ad1978ec3092cce48f16456aa820bf5786a101a8378323659ed11db3",
    "zh:fcf5ad18fafe717739c5d40d8c4e4a70e123cf4296efc7286f9d98e3c42e410f",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.6.1"
  constraints = ">= 2.0.2"
  hashes = [
    "h1:aw4mTyVx41Y/+lAGNJcRvylF2j0sRQD6up5/CXmj9ds=",
    "zh:081fbaf9441ebb278753dcf05f318fa7d445e9599a600d7c525e9a18b871d4c8",
    "zh:143bfbe871c628981d756ead47486e807fce876232d05607e0b8852ebee4eed8",
    "zh:34f413a644eb952e3f041d67ef19200f4c286d374eae87b60fafdd8bf6bb5654",
    "zh:370562be70233be730e1876d565710c3ef477e047f209cb3dff8a4a3217a6461",
    "zh:443021df6d56e59e4d8dda8e57b506affff32b8a22de09661d21b98bc781fefb",
    "zh:51a9501360b58adf9ee6e09fb81f555042ebc909ab36e06ccfc5e701e91f9923",
    "zh:7d41d48b8291b98e0a4b7a1f79a9d1fe140a2e0d8df422c5b48cbae4c3fa615a",
    "zh:881b3e44814d7d49a5820e2e4b13ee3d000b5baf7957df774a909f17472ece8a",
    "zh:b860ff68a944de63fbe0a624c41f2e373711a2da4298c0f0cb151e00fb32a6b3",
    "zh:c4ab48ea6e0f8d4a6db1abab1877addb2b21ecd126e505c74b8c85804bd92cbe",
    "zh:e96589575dfd31eab48fcc85466dd49895925473c60c802b346cdb4037953350",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:PaQTpxHMbZB9XV+c1od1eaUvndQle3ZZHx79hrI6C3k=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.0"
  hashes = [
    "h1:grYDj8/Lvp1OwME+g1AsECPN1czO5ssSf+8fCluCHQY=",
    "zh:02a1675fd8de126a00460942aaae242e65ca3380b5bb192e8773ef3da9073fd2",
    "zh:53e30545ff8926a8e30ad30648991ca8b93b6fa496272cd23b26763c8ee84515",
    "zh:5f9200bf708913621d0f6514179d89700e9aa3097c77dac730e8ba6e5901d521",
    "zh:9ebf4d9704faba06b3ec7242c773c0fbfe12d62db7d00356d4f55385fc69bfb2",
    "zh:a6576c81adc70326e4e1c999c04ad9ca37113a6e925aefab4765e5a5198efa7e",
    "zh:a8a42d13346347aff6c63a37cda9b2c6aa5cc384a55b2fe6d6adfa390e609c53",
    "zh:c797744d08a5307d50210e0454f91ca4d1c7621c68740441cf4579390452321d",
    "zh:cecb6a304046df34c11229f20a80b24b1603960b794d68361a67c5efe58e62b8",
    "zh:e1371aa1e502000d9974cfaff5be4cfa02f47b17400005a16f14d2ef30dc2a70",
    "zh:fc39cc1fe71234a0b0369d5c5c7f876c71b956d23d7d6f518289737a001ba69b",
    "zh:fea4227271ebf7d9e2b61b89ce2328c7262acd9fd190e1fd6d15a591abfa848e",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version     = "3.1.0"
  constraints = "3.1.0"
  hashes = [
    "h1:U+kgPLboCrcs4eZV87esP7iydF8mjMyHKE/mDsrwfkQ=",
    "zh:3d46616b41fea215566f4a957b6d3a1aa43f1f75c26776d72a98bdba79439db6",
    "zh:623a203817a6dafa86f1b4141b645159e07ec418c82fe40acd4d2a27543cbaa2",
    "zh:668217e78b210a6572e7b0ecb4134a6781cc4d738f4f5d09eb756085b082592e",
    "zh:95354df03710691773c8f50a32e31fca25f124b7f3d6078265fdf3c4e1384dca",
    "zh:9f97ab190380430d57392303e3f36f4f7835c74ea83276baa98d6b9a997c3698",
    "zh:a16f0bab665f8d933e95ca055b9c8d5707f1a0dd8c8ecca6c13091f40dc1e99d",
    "zh:be274d5008c24dc0d6540c19e22dbb31ee6bfdd0b2cddd4d97f3cd8a8d657841",
    "zh:d5faa9dce0a5fc9d26b2463cea5be35f8586ab75030e7fa4d4920cd73ee26989",
    "zh:e9b672210b7fb410780e7b429975adcc76dd557738ecc7c890ea18942eb321a5",
    "zh:eb1f8368573d2370605d6dbf60f9aaa5b64e55741d96b5fb026dbfe91de67c0d",
    "zh:fc1e12b713837b85daf6c3bb703d7795eaf1c5177aebae1afcf811dd7009f4b0",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.44.0"
  constraints = "1.44.0"
  hashes = [
    "h1:7B+VfdX20R5IXjOP+jiUK9vHtSUHJYY9I5X69e1Cav4=",
    "zh:0bc1d27bfec4fc30adc7f804ed6af01752c07005ed7689e7a66f213b12de49ad",
    "zh:182b243a0378c8d7447b92f18ab6b7b48292bdb2c4ab8c489250e6c118692bf7",
    "zh:3fe0dfdfb51dd7a862335701d3bcf4988a6383b5f3ef408d8c7801df7beb75c0",
    "zh:4a3ade98c5b6bd24829c581a9f2f1090be71eb9020ebaf873a6cd625fc29a3fb",
    "zh:4eb6753a4b3d796f35c5b2e78070afdf95b37b1b06b5f5ff5919d53e626b75fc",
    "zh:7a9180f98fdbb94e974a08e1aa080e362ed5e0954a9d61bed4eb8e1312e81612",
    "zh:877e06e4403e4156c4f5afcdb495be3ab61c20a6a784ffc402ec8beeadd43a79",
    "zh:88ab7ab7ecef6d14cda82ddc3d11baf97ef0d13df73c07183039ba0a517d9d3f",
    "zh:8944f4764821a6954466b2da34f69f77ed0112e3c4d83e8144bf762379178b32",
    "zh:9c25fe097fb41a309c2493ccb0ee33680308e5269f8c609dfc68eec51ca21dc3",
    "zh:9f37466d691e22c923e3f1d256a49394ba7fcfec852f3c20ff86b81b32f06ec2",
    "zh:bb2b1b59b2f52e0c48dcbc7322876eeb3b4e16b4f6c6af7f8e29a7f893e53611",
    "zh:c2592631f429dcb35f9829ec7bd381d9f75d1d0bd6a82deb183d816280c72850",
    "zh:cc4ea9209813db615226523b0018c0919f70471ca29992529e0860865dfad267",
  ]
}
