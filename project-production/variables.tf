variable "gitlab_username" {
  type = string
}

variable "gitlab_token" {
  type = string
}