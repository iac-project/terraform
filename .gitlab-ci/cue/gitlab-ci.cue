 let TerraformModules = [
	'k0s-cluster',
	'k0s-config-generator',
	'openstack-cluster',
	'flux',
]

{for module in TerraformModules {
	"\(module)": {
		stage: "test"
		image: "akselleirv/terratest:0.0.9"
		script: [
			"cd modules/\(module)/test",
			"go test -v -timeout 30m 2>&1 | tee test_output.log",
			"terratest_log_parser -testlog test_output.log -outputdir test_output",
			"cat test_output.log | go-junit-report > report.xml",
		]
		artifacts: {
			paths: [
				"modules/\(module)/test/test_output",
			]
			reports: junit: "modules/\(module)/test/report.xml"
		}
		cache: {
			key: "go-cache"
			paths: [
				"$GOPATH/go/pkg",
			]
		}
	}
}
}
