terraform {
  required_version = ">= 0.13"

  required_providers {

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }

    flux = {
      source  = "fluxcd/flux"
      version = ">= 0.0.13"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.2"
    }

  }
}
locals {
  kube_config_decoded = {
    host                   = var.kube_config.host
    client_certificate     = base64decode(var.kube_config.client_certificate)
    cluster_ca_certificate = base64decode(var.kube_config.cluster_ca_certificate)
    client_key             = base64decode(var.kube_config.client_key)
  }
}

provider "kubectl" {
  host                   = local.kube_config_decoded.host
  client_certificate     = local.kube_config_decoded.client_certificate
  cluster_ca_certificate = local.kube_config_decoded.cluster_ca_certificate
  client_key             = local.kube_config_decoded.client_key
  load_config_file       = false
}

provider "kubernetes" {
  host                   = local.kube_config_decoded.host
  client_certificate     = local.kube_config_decoded.client_certificate
  cluster_ca_certificate = local.kube_config_decoded.cluster_ca_certificate
  client_key             = local.kube_config_decoded.client_key
}

locals {
  target_path = "./clusters/${var.cluster_name}"
}

data "flux_install" "main" {
  target_path = local.target_path
}

data "flux_sync" "main" {
  target_path = local.target_path
  url         = "https://gitlab.stud.idi.ntnu.no/iac-project/kubernetes.git"
  branch      = "main"
}

resource "kubernetes_namespace" "flux_system" {
  metadata {
    name = "flux-system"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

data "kubectl_file_documents" "install" {
  content = data.flux_install.main.content
}

data "kubectl_file_documents" "sync" {
  content = data.flux_sync.main.content
}


locals {
  install = [
    for v in data.kubectl_file_documents.install.documents : {
      data : yamldecode(v)
      content : v
    }
  ]
  sync = [
    for v in data.kubectl_file_documents.sync.documents : {
      data : yamldecode(v)
      content : v
    }
  ]
}

resource "kubectl_manifest" "install" {
  for_each = {
    for v in local.install : lower(join("/", compact([
      v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name
    ]))) => v.content
  }
  depends_on = [kubernetes_namespace.flux_system]
  yaml_body  = each.value
}

resource "kubectl_manifest" "sync" {
  for_each = {
    for v in local.sync : lower(join("/", compact([
      v.data.apiVersion, v.data.kind, lookup(v.data.metadata, "namespace", ""), v.data.metadata.name
    ]))) => v.content
  }
  depends_on = [kubernetes_namespace.flux_system]
  yaml_body  = each.value
}

resource "kubernetes_secret" "main" {
  depends_on = [kubectl_manifest.install]

  metadata {
    name      = data.flux_sync.main.secret
    namespace = data.flux_sync.main.namespace
  }

  data = {
    username = var.gitlab_username
    password = var.gitlab_token
  }
}