variable "cluster_name" {
  type        = string
  description = "Name of the cluster."
}

variable "kube_config" {
  type = object({
    host                   = string
    client_certificate     = string
    cluster_ca_certificate = string
    client_key             = string
  })
  description = "Credentials used to authenticate with kubernetes api"
}

variable "gitlab_username" {
  type        = string
  description = "Username used to authenticate with Gitlab."
}

variable "gitlab_token" {
  type        = string
  description = "Token used to authenticate with Gitlab."
}