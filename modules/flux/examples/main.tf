module "k0s_cluster" {
  source = "../../k0s-cluster"

  cluster_name         = var.cluster_name
  rsa_private_key_path = var.rsa_private_key_path
  public_key_openssh   = var.public_key_openssh
}

module "flux" {
  source = "../"

  cluster_name = "flux-test-cluster"
  kube_config  = module.k0s_cluster.kubeconfig

  gitlab_username = var.gitlab_username
  gitlab_token    = var.gitlab_token
}