package test

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"github.com/gruntwork-io/terratest/modules/k8s"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	gossh "golang.org/x/crypto/ssh"
	"os"
	"strings"
	"testing"
	"time"
)

func TestFlux(t *testing.T) {
	privateKeyPath, publicKeyOpenSSH, cleanupRSAKeys := generateTempRSAKey(t)
	defer cleanupRSAKeys()
	tfOpts := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../examples",
		Vars: map[string]interface{}{
			"cluster_name":         "k0s-cluster-test-" + strings.ToLower(random.UniqueId()),
			"public_key_openssh":   publicKeyOpenSSH,
			"rsa_private_key_path": privateKeyPath,
		},
	})
	defer terraform.Destroy(t, tfOpts)
	terraform.InitAndApply(t, tfOpts)
	validateFlux(t, terraform.Output(t, tfOpts, "kubeconfig_path"))
}

func validateFlux(t *testing.T, kubeConfigFilePath string) {
	fluxTestNs := "flux-test"
	k8sOpts := k8s.NewKubectlOptions("", kubeConfigFilePath, fluxTestNs)
	k8s.CreateNamespace(t, k8sOpts, fluxTestNs)
	defer k8s.DeleteNamespace(t, k8sOpts, fluxTestNs)
	k8s.WaitUntilPodAvailable(t, k8sOpts, "nginx", 15, 5*time.Second)
}

func generateTempRSAKey(t *testing.T) (privateKeyPath string, publicKeyOpenSSH string, cleanupKeys func()) {
	rsaKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		t.Fatal(err)
	}
	privateKeyPath = os.TempDir() + "/rsa_private_key"
	writeFile(t, privateKeyPath, privateKeyToBytes(rsaKey))
	publicKeyOpenSSH = "ssh-rsa " + rsaKeyToOpenSSHFormat(t, rsaKey)

	return privateKeyPath, publicKeyOpenSSH, func() {
		err = os.RemoveAll(privateKeyPath)
		if err != nil {
			t.Fatal(err)
		}
	}
}

func rsaKeyToOpenSSHFormat(t *testing.T, rsaKey *rsa.PrivateKey) string {
	pub, err := gossh.NewPublicKey(&rsaKey.PublicKey)
	if err != nil {
		t.Fatal(err)
	}
	return base64.StdEncoding.EncodeToString(pub.Marshal())
}

func privateKeyToBytes(priv *rsa.PrivateKey) []byte {
	privBytes := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)
	return privBytes
}

func writeFile(t *testing.T, path string, data []byte) {
	err := os.WriteFile(path, data, 0666)
	if err != nil {
		t.Fatal(err)
	}
}

func mustGetEnv(t *testing.T, key string) string {
	v := os.Getenv(key)
	if v == "" {
		t.Fatal(fmt.Errorf("could not find any value for env key %s", key))
	}
	return v
}
