output "ips" {
  value = [
    for i, ip in openstack_compute_floatingip_associate_v2.fip_1 : ip.floating_ip
  ]
}

output "cluster_name" {
  value = var.cluster_name
}