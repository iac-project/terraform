package test

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/retry"
	"github.com/gruntwork-io/terratest/modules/ssh"
	"github.com/gruntwork-io/terratest/modules/terraform"
	gossh "golang.org/x/crypto/ssh"
	"strings"
	"testing"
	"time"
)

func TestOpenstackCluster(t *testing.T) {
	rsaKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		t.Fatal(err)
	}
	opts := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../examples",
		Vars: map[string]interface{}{
			"public_key":   "ssh-rsa " + rsaKeyToOpenSSHFormat(t, rsaKey),
			"cluster_name": "openstack-cluster-test-" + random.UniqueId(),
		},
	})
	defer terraform.Destroy(t, opts)
	terraform.InitAndApply(t, opts)

	ips := terraform.OutputList(t, opts, "ip")

	for _, ip := range ips {
		sshToPrivateHost(t, ip, rsaKey)
	}
}

func rsaKeyToOpenSSHFormat(t *testing.T, rsaKey *rsa.PrivateKey) string {
	pub, err := gossh.NewPublicKey(&rsaKey.PublicKey)
	if err != nil {
		t.Fatal(err)
	}
	return base64.StdEncoding.EncodeToString(pub.Marshal())
}

func sshToPrivateHost(t *testing.T, ip string, privKey *rsa.PrivateKey) {

	host := ssh.Host{
		Hostname:    ip,
		SshKeyPair:  &ssh.KeyPair{PrivateKey: privateKeyToString(privKey), PublicKey: publicKeyToString(t, &privKey.PublicKey)},
		SshUserName: "ubuntu",
	}

	// It can take a minute or so for the Instance to boot up, so retry a few times
	maxRetries := 30
	timeBetweenRetries := 5 * time.Second
	description := fmt.Sprintf("SSH to host %s", ip)

	// Run a simple echo command on the server
	expectedText := "Hello, World"
	command := fmt.Sprintf("echo -n '%s'", expectedText)

	// Verify that we can SSH to the Instance and run commands
	retry.DoWithRetry(t, description, maxRetries, timeBetweenRetries, func() (string, error) {

		actualText, err := ssh.CheckSshCommandE(t, host, command)

		if err != nil {
			return "", err
		}

		if strings.TrimSpace(actualText) != expectedText {
			return "", fmt.Errorf("expected SSH command to return '%s' but got '%s'", expectedText, actualText)
		}

		return "", nil
	})

}

func publicKeyToString(t *testing.T, pub *rsa.PublicKey) string {
	pubASN1, err := x509.MarshalPKIXPublicKey(pub)
	if err != nil {
		t.Fatal(err)
	}

	pubBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: pubASN1,
	})
	return string(pubBytes)
}

func privateKeyToString(priv *rsa.PrivateKey) string {
	privBytes := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)

	return string(privBytes)
}
