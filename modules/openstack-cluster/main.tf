terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.44.0"
    }
  }
}


resource "openstack_networking_network_v2" "network_1" {
  name           = "${var.cluster_name}_net1"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_1" {
  name       = "${var.cluster_name}_subnet1"
  network_id = openstack_networking_network_v2.network_1.id
  cidr       = "192.168.100.0/24"
  ip_version = 4
}

resource "openstack_networking_router_v2" "router_1" {
  name                = "${var.cluster_name}_router1"
  admin_state_up      = true
  external_network_id = "730cb16e-a460-4a87-8c73-50a2cb2293f9" // ntnu-internal ID
}

resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.router_1.id
  subnet_id = openstack_networking_subnet_v2.subnet_1.id
}

resource "openstack_compute_instance_v2" "basic" {
  count           = var.number_of_nodes
  name            = "${var.cluster_name}_vm${count.index}"
  image_id        = "2dcc56bf-6f1e-47aa-8b6d-4096009d9c30" // Ubuntu 20.04 LTS
  flavor_id       = "f9d74bd7-38be-4b90-9b09-1db9d8a83cf1" // m1.small
  key_pair        = openstack_compute_keypair_v2.test-keypair.name
  security_groups = [openstack_compute_secgroup_v2.secgroup_1.name]

  network {
    uuid = openstack_networking_subnet_v2.subnet_1.network_id
  }
}

resource "openstack_compute_secgroup_v2" "secgroup_1" {
  name        = "${var.cluster_name}_secgroup"
  description = "This security group was created by the OpenStack terraform module."

  dynamic "rule" {
    for_each = [2380, 6443, 179, 10250, 9443, 8132, 22]
    content {
      from_port   = rule.value
      to_port     = rule.value
      ip_protocol = "tcp"
      cidr        = "0.0.0.0/0"
    }
  }
  rule {
    from_port   = 4789
    to_port     = 4789
    ip_protocol = "udp"
    cidr        = "0.0.0.0/0"
  }

}

resource "openstack_compute_floatingip_v2" "floatip_1" {
  pool  = "ntnu-internal"
  count = var.number_of_nodes
}

resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  count       = length(openstack_compute_instance_v2.basic)
  floating_ip = openstack_compute_floatingip_v2.floatip_1[count.index].address
  instance_id = openstack_compute_instance_v2.basic[count.index].id
}

resource "openstack_compute_keypair_v2" "test-keypair" {
  name       = "${var.cluster_name}-test-key"
  public_key = var.public_key
}

