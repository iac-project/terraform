variable "public_key" {
  type        = string
  description = "public key corresponding to private key"
}

variable "cluster_name" {
  type        = string
  description = "Name of the cluster. This name should be unique to not conflict with any other names in your OpenStack account."
}