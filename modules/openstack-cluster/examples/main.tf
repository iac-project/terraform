module "openstack_cluster" {
  source          = "../"
  cluster_name    = var.cluster_name
  number_of_nodes = 2
  public_key      = var.public_key
}