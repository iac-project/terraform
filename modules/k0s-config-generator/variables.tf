variable "cluster_name" {
  type        = string
  description = "Cluster name which is the same name as used in openstack cluster"
}

variable "ips" {
  type        = list(string)
  description = "A list of the IPs of the machines"
}

variable "rsa_private_key_path" {
  type        = string
  description = "Path to the RSA private key."
}