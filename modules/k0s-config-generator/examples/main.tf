module "k0s_config_generator" {
  source = "../"

  cluster_name         = var.cluster_name
  ips                  = ["10.0.0.69", "10.0.0.70"]
  rsa_private_key_path = var.rsa_private_key_path
}