variable "rsa_private_key_path" {
  type        = string
  description = "Path to the RSA private key."
}
variable "cluster_name" {
  type        = string
  description = "Cluster name which is the same name as used in openstack cluster"
}