resource "local_file" "k0s_config" {
  content = templatefile("${path.module}/resources/k0s-tmpl.yaml", {
    ips                  = var.ips,
    rsa_private_key_path = var.rsa_private_key_path,
    cluster_name         = var.cluster_name
  })
  filename = "/tmp/k0s-${var.cluster_name}.yaml"
}