package test

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
	"os"
	"os/exec"
	"testing"
	"time"
)

func TestK0sConfigGenerator(t *testing.T) {
	rsaKeyPath, cleanupRSAKey := generateTempRSAKey(t)
	defer cleanupRSAKey()
	tfOpts := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../examples",
		Vars: map[string]interface{}{
			"cluster_name":         "k0s-apply-config-generator",
			"rsa_private_key_path": rsaKeyPath,
		},
	})
	defer terraform.Destroy(t, tfOpts)
	terraform.InitAndApply(t, tfOpts)

	filePath := terraform.Output(t, tfOpts, "file_path")
	k0sctlApply(t, filePath)
}

func k0sctlApply(t *testing.T, filePath string) {
	cmd := exec.Command("k0sctl", "apply", "--config", filePath)

	output, err := runCommandForSeconds(t, cmd, 5*time.Second)
	if err != nil {
		t.Fatalf("error with command: %s", err.Error())
	}
	logger.Log(t, output)
	assert.Contains(t, output, "Running phase: Connect to hosts")
}

func runCommandForSeconds(t *testing.T, cmd *exec.Cmd, timeout time.Duration) (string, error) {
	done := make(chan error, 1)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	logger.Log(t, "running command: ", cmd.Args)
	if err := cmd.Start(); err != nil {
		t.Fatalf("error with excuting command: %s: %s", err.Error(), stderr.String())
	}

	go func() {
		done <- cmd.Wait()
	}()

	select {
	case <-time.After(timeout):
		if err := cmd.Process.Kill(); err != nil {
			t.Fatal("failed to kill process: ", err)
		}
		logger.Log(t, "process killed as timeout reached")
		return stdout.String(), nil
	case err := <-done:
		if err != nil {
			return "", fmt.Errorf("process exited with error: %w: %s: %s", err, stderr.String(), stdout.String())
		}
		return "", fmt.Errorf("process early, this is an error: %w: %s: %s", err, stderr.String(), stdout.String())
	}
}

func generateTempRSAKey(t *testing.T) (string, func()) {
	rsaKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		t.Fatal(err)
	}
	tempPath := os.TempDir() + "/rsa_temp_key"
	err = os.WriteFile(tempPath, privateKeyToBytes(rsaKey), 0666)
	if err != nil {
		t.Fatal(err)
	}
	return tempPath, func() {
		err = os.RemoveAll(tempPath)
		if err != nil {
			t.Fatal(err)
		}
	}
}
func privateKeyToBytes(priv *rsa.PrivateKey) []byte {
	privBytes := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)
	return privBytes
}
