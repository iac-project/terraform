#!/usr/bin/env bash

set -eu

mkdir /tmp/k0sctl-apply || true
cd /tmp/k0sctl-apply

# Download k0sctl
wget https://github.com/k0sproject/k0sctl/releases/download/v0.11.3/k0sctl-linux-x64
mv ./k0sctl-linux-x64 k0sctl
chmod +x k0sctl

# Apply config
echo "applying with config: $K0S_CONFIG_PATH"
./k0sctl apply --config "$K0S_CONFIG_PATH"

# Save KubeConfig
./k0sctl kubeconfig --config "$K0S_CONFIG_PATH" > /tmp/"$CLUSTER_NAME"