variable "k0s_config_filepath" {
  type        = string
  description = "The generated k0s config file to be applied."
}

variable "cluster_name" {
  type        = string
  description = "Name of the cluster."
}