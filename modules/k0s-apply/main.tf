resource "null_resource" "k0s" {

  provisioner "local-exec" {
    when        = create
    interpreter = ["bash", "-c"]

    command = file("${path.module}/scripts/apply.sh")

    environment = {
      K0S_CONFIG_PATH = var.k0s_config_filepath
      CLUSTER_NAME    = var.cluster_name
    }
  }
}