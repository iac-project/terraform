module "k0s_cluster" {
  source = "../"

  cluster_name         = var.cluster_name
  public_key_openssh   = var.public_key_openssh
  rsa_private_key_path = var.rsa_private_key_path
}
