variable "cluster_name" {
  type        = string
  description = "Name of the cluster."
}

variable "rsa_private_key_path" {
  type        = string
  description = "The path to the private key which can connect to the VM instances."
}

variable "public_key_openssh" {
  type        = string
  description = "The public key to be used to SSH to the Openstack instances."
}