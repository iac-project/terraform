module "openstack_cluster" {
  source          = "../openstack-cluster"
  cluster_name    = var.cluster_name
  number_of_nodes = 2
  public_key      = var.public_key_openssh
}

module "k0s_config_generator" {
  source               = "../k0s-config-generator"
  cluster_name         = module.openstack_cluster.cluster_name
  ips                  = module.openstack_cluster.ips
  rsa_private_key_path = var.rsa_private_key_path
}

module "k0s_apply" {
  source              = "../k0s-apply"
  cluster_name        = var.cluster_name
  k0s_config_filepath = module.k0s_config_generator.file_path
}

locals {
  kubeconfig_raw = yamldecode(data.local_file.kubeconfig.content)
  kubeconfig = {
    host                   = local.kubeconfig_raw.clusters.0.cluster.server
    client_certificate     = local.kubeconfig_raw.users.0.user.client-certificate-data
    cluster_ca_certificate = local.kubeconfig_raw.clusters.0.cluster.certificate-authority-data
    client_key             = local.kubeconfig_raw.users.0.user.client-key-data
  }
}

data "local_file" "kubeconfig" {
  filename   = module.k0s_apply.kubeconfig_path
  depends_on = [module.k0s_apply]
}
