package test

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"github.com/gruntwork-io/terratest/modules/k8s"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	testStructure "github.com/gruntwork-io/terratest/modules/test-structure"
	gossh "golang.org/x/crypto/ssh"
	"os"
	"strings"
	"testing"
)

const tfOptsPath = "/tmp/k0s-cluster"

func TestK0sCluster(t *testing.T) {
	privateKeyPath, publicKeyOpenSSH, cleanupRSAKeys := generateTempRSAKey(t)
	tfOpts := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../examples",
		Vars: map[string]interface{}{
			"cluster_name":         "k0s-cluster-test-" + strings.ToLower(random.UniqueId()),
			"public_key_openssh":   publicKeyOpenSSH,
			"rsa_private_key_path": privateKeyPath,
		},
	})

	defer testStructure.RunTestStage(t, "teardown", func() {
		terraform.Destroy(t, tfOpts)
		cleanupRSAKeys()
	})

	testStructure.RunTestStage(t, "load", func() {
		tfOpts = loadTerraformOptions(t, tfOpts)
	})

	testStructure.RunTestStage(t, "setup", func() {
		terraform.InitAndApply(t, tfOpts)
		testStructure.SaveTerraformOptions(t, tfOptsPath, tfOpts)
	})

	k8sOpts := k8s.NewKubectlOptions("", terraform.Output(t, tfOpts, "kubeconfig_path"), "default")
	k8s.GetNodes(t, k8sOpts)
}

func generateTempRSAKey(t *testing.T) (privateKeyPath string, publicKeyOpenSSH string, cleanupKeys func()) {
	rsaKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		t.Fatal(err)
	}
	privateKeyPath = os.TempDir() + "/rsa_private_key"
	writeFile(t, privateKeyPath, privateKeyToBytes(rsaKey))

	publicKeyOpenSSH = "ssh-rsa " + rsaKeyToOpenSSHFormat(t, rsaKey)

	return privateKeyPath, publicKeyOpenSSH, func() {
		err = os.RemoveAll(privateKeyPath)
		if err != nil {
			t.Fatal(err)
		}
	}
}

func rsaKeyToOpenSSHFormat(t *testing.T, rsaKey *rsa.PrivateKey) string {
	pub, err := gossh.NewPublicKey(&rsaKey.PublicKey)
	if err != nil {
		t.Fatal(err)
	}
	return base64.StdEncoding.EncodeToString(pub.Marshal())
}

func privateKeyToBytes(priv *rsa.PrivateKey) []byte {
	privBytes := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)
	return privBytes
}

func writeFile(t *testing.T, path string, data []byte) {
	err := os.WriteFile(path, data, 0666)
	if err != nil {
		t.Fatal(err)
	}
}

// loadTerraformOptions returns the saved options if present else it returns the options passed.
func loadTerraformOptions(t *testing.T, opts *terraform.Options) *terraform.Options {
	if testStructure.IsTestDataPresent(t, tfOptsPath+"/.test-data/TerraformOptions.json") {
		opts = testStructure.LoadTerraformOptions(t, tfOptsPath)
	}
	return opts
}
