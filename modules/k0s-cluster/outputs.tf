output "kubeconfig_path" {
  value = module.k0s_apply.kubeconfig_path
}

output "kubeconfig" {
  value = local.kubeconfig
}