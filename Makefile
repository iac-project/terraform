build-gitlab-ci:
	rm -rf .gitlab-ci/generated-ci \
	&& mkdir .gitlab-ci/generated-ci \
	&& cue export .gitlab-ci/cue/gitlab-ci.cue --out yaml > .gitlab-ci/generated-ci/.gitlab-ci.yml